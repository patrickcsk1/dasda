import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as firebase from "firebase";
import * as firebase2 from "firebase";
import "firebase/auth";
import "firebase/firestore";

firebase.initializeApp({
  apiKey: "AIzaSyBI8v33cJP63WyTkaldyDCOYnRn3NGSj30",
  authDomain: "asistencia-web-118a5.firebaseapp.com",
  databaseURL: "https://asistencia-web-118a5.firebaseio.com",
  projectId: "asistencia-web-118a5",
  storageBucket: "asistencia-web-118a5.appspot.com",
  messagingSenderId: "1095901395451",
  appId: "1:1095901395451:web:759567d9e5dc59ac20335e"
});

firebase2.initializeApp({
  apiKey: "AIzaSyBI8v33cJP63WyTkaldyDCOYnRn3NGSj30",
  authDomain: "asistencia-web-118a5.firebaseapp.com",
  databaseURL: "https://asistencia-web-118a5.firebaseio.com",
  projectId: "asistencia-web-118a5",
  storageBucket: "asistencia-web-118a5.appspot.com",
  messagingSenderId: "1095901395451",
  appId: "1:1095901395451:web:759567d9e5dc59ac20335e"
},"firebase2");

ReactDOM.render(<App />, document.getElementById("root"));
