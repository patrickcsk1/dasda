import React, { Component } from "react";
import "../styles/navbar.css";
import logo from "../assets/logo.jpg";

export default class Navbar extends Component {
  render() {
    return (
      <div className="nav-def">
        <nav className="nav-bar">
          <div className="row">
            <div className="col-2">
              <img className="img-nav" src={logo} alt="Logo"></img>
            </div>
            <div className="col-7">
              <h1> Bienvenido(a) {this.props.nombre}</h1>
            </div>
            <div className="col-3">
              <label onClick={this.props.clickeo}> <u>Cerrar Sesión</u></label>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
