import React, { Component } from "react";
import firebase from "firebase";
import swal from "sweetalert2";
import ReactModal from "react-modal";
import "../styles/attendance.css";

// Miembro fantasma : idmember I3njvPlOvgvCfFUbXRMz en Brenda

export default class Attendance extends Component {
  constructor(props) {
    super(props);

    this.firebase = firebase;
    this.swal = swal;

    this.closeModal = this.closeModal.bind(this);
    this.closeModalDetail = this.closeModalDetail.bind(this);
    this.initRegAsistencia = this.initRegAsistencia.bind(this);
    this.hallarAsistencia = this.hallarAsistencia.bind(this);
    // this.getMiembros = this.getMiembros.bind(this);
    this.getDias = this.getDias.bind(this);
    this.getFechas = this.getFechas.bind(this);
    this.getInfo = this.getInfo.bind(this);
    this.takeList = this.takeList.bind(this);
    this.tomarAsistencia = this.tomarAsistencia.bind(this);

    this.state = {
      showModal: false,
      showModalDetail: false,
      lider: this.props.lider,
      diaSelected: "",
      dias: [],
      fechas: [],
      // miembros: [],
      miembros: this.props.miembros,
      listado: [],
      asistencias: []
    };
  }

  static getDerivedStateFromProps(props, state) {
      console.log("Actualizacion de miembros: " + props.miembros.length);
      console.log("Actuales miembros: " + state.miembros.length);
      return {
        miembros: props.miembros,
      };
  }

  // componentDidUpdate(oldProps) {
  //   const newProps = this.props
  //   if(oldProps.miembros !== newProps.miembros) {
  //     this.setState({ miembros: newProps.miembros });
  //   }
  // }

  async getDias() {
    await firebase
      .firestore()
      .collection("dias")
      .get()
      .then(async snapshot => {
        await snapshot.forEach(doc => {
          var obj = {
            id: doc.id,
            data: doc.data()
          };
          this.setState({
            dias: [...this.state.dias, obj]
          });
          return;
        });
      }, await this.getFechas())
      .catch(e => {
        console.log("GetDias: " + e.message);
      });
  }

  async getFechas() {
    await firebase
      .firestore()
      .collection("fechas")
      .get()
      .then(async snapshot => {
        await snapshot.forEach(doc => {
          var obj = {
            id: doc.id,
            data: doc.data()
          };
          this.setState({
            fechas: [...this.state.fechas, obj]
          });
          return;
        });
      // }, await this.getMiembros())
      })
      .catch(e => {
        console.log("GetFechas: " + e.message);
      });
  }

  hallarAsistencia(idMember, liderUID, idFecha, idDia) {
    var aux = this.state.asistencias.find(
      asistencia =>
        asistencia.data.idMember === idMember &&
        asistencia.data.uidLider === liderUID &&
        asistencia.data.idFecha === idFecha &&
        asistencia.data.idDia === idDia
    );
    return aux;
  }

  pushObjAsistencia(obj) {
    this.setState({
      asistencias: [...this.state.asistencias, obj]
    });
  }

  async agregarAsistencia(fecha, miembro) {
    await this.firebase
      .firestore()
      .collection("asistencia")
      .add({
        idDia: fecha.data.idDia,
        idFecha: fecha.id,
        idMember: miembro.id,
        uidLider: miembro.data.liderUID,
        asis: false
      })
      .then(ref => {
        var datad = {
          idDia: fecha.data.idDia,
          idFecha: fecha.id,
          idMember: miembro.id,
          uidLider: miembro.data.liderUID,
          asis: false
        };
        var obj = {
          id: ref.id,
          data: datad
        };
        this.pushObjAsistencia(obj);
      })
      .catch(e => console.log("Agregar Asistencia: " + e.message));
  }

  async estaRegistro(fecha, miembro) {
    var estaLocal = false;
    var obj = null;
    if (!this.asistencias) estaLocal = false;
    else {
      obj = this.hallarAsistencia(
        miembro.id,
        this.state.lider.uid,
        fecha.id,
        fecha.data.idDia
      );
      if (obj.data) return obj;
      else estaLocal = false;
    }
    if (estaLocal === false) {
      //puede estar en el firestore
      var registro = await this.firebase
        .firestore()
        .collection("asistencia")
        .where("idDia", "==", fecha.data.idDia)
        .where("idFecha", "==", fecha.id)
        .where("idMember", "==", miembro.id)
        .where("uidLider", "==", miembro.data.liderUID)
        .get();
      if (registro.empty || registro.size === 0) {
        return null;
      } else {
        obj = {
          id: registro.docs[0].id,
          data: registro.docs[0].data()
        };
        return obj;
      }
    }
  }

  initRegAsistencia() {
    this.state.fechas.map(fecha => {
      this.state.miembros.map(async miembro => {
        var obj = await this.estaRegistro(fecha, miembro);
        if (obj === null) {
          await this.agregarAsistencia(fecha, miembro);
        } else {
          this.pushObjAsistencia(obj);
        }
      });
    });
  }

  async componentDidMount() {
    await this.getDias();
    console.log(this.state.dias.length);
    console.log(this.state.fechas.length);
    console.log(this.state.miembros.length);
    this.initRegAsistencia();
  }

  getInfo(id) {
    // console.log("Info de clase: " + id);
    this.setState({ diaSelected: id, showModalDetail: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  closeModalDetail() {
    this.setState({ showModalDetail: false });
  }

  takeList(id) {
    this.setState({ diaSelected: id, showModal: true });
  }

  diasList() {
    return this.state.dias.map((currentDay, i) => {
      return (
        <tr key={i}>
          <td>{currentDay.data.descripcion}</td>
          <td>
            <button
              type="button"
              className="btn detalle-btn"
              onClick={() => {
                this.getInfo(currentDay.id);
              }}
            >
              Detalle
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn asistencia-btn"
              onClick={() => {
                this.takeList(currentDay.id);
              }}
            >
              Tomar Lista
            </button>
          </td>
        </tr>
      );
    });
  }

  headersFecha() {
    var lista = this.state.fechas.filter(
      fecha => fecha.data.idDia === this.state.diaSelected
    );
    return lista.map((currentDate, i) => {
      return <th key={i}>{currentDate.data.fecha}</th>;
    });
  }

  tomarAsistencia(event, idMember, liderUID, idFecha, idDia) {
    var checkedValue = event.target.checked;
    var dato = this.hallarAsistencia(idMember, liderUID, idFecha, idDia);
    if (dato.data.asis !== checkedValue) {
      this.swal
        .fire({
          title: "Aviso",
          text: "Seguro que quieres modificar la asistencia?",
          icon: "warning",
          showCancelButton: true,
          cancelButtonText: "Cancelar",
          confirmButtonText: "Aceptar"
        })
        .then(result => {
          if (result.value) {
            this.firebase
              .firestore()
              .collection("asistencia")
              .doc(dato.id)
              .update({ asis: checkedValue })
              .then(() => {
                var index = this.state.asistencias
                  .map(data => {
                    return data.id;
                  })
                  .indexOf(dato.id);
                var auxAsistencia = [...this.state.asistencias];
                if (index !== -1) {
                  var objeto = auxAsistencia[index];
                  // console.log(checkedValue);
                  objeto.data.asis = checkedValue;
                  // console.log(objeto.data.asis);
                  auxAsistencia[index] = objeto;
                  // console.log(auxAsistencia[index]);
                  this.setState({ asistencias: auxAsistencia });
                  this.swal.fire({
                    title: "Aviso",
                    text: "Operación exitosa",
                    icon: "success"
                  });
                }
              })
              .catch(e => {
                console.log("TomarAsistencia error: " + e.message);
              });
          }
        });
    }
  }

  valorDefault(idMember, liderUID, idFecha, idDia) {
    // console.log("Valor Default");
    var valor = this.hallarAsistencia(idMember, liderUID, idFecha, idDia);
    // console.log(valor);
    return valor.data.asis;
  }

  auxFunctionBox(idMember, liderUID, flag) {
    var lista = this.state.fechas.filter(
      fecha => fecha.data.idDia === this.state.diaSelected
    );
    return lista.map((currentDate, i) => {
      return (
        <td key={i}>
          <input
            type="checkbox"
            checked={this.valorDefault(
              idMember,
              liderUID,
              currentDate.id,
              currentDate.data.idDia
            )}
            onChange={async e =>
              await this.tomarAsistencia(
                e,
                idMember,
                liderUID,
                currentDate.id,
                currentDate.data.idDia,
                flag
              )
            }
            disabled={flag === 1}
          ></input>
        </td>
      );
    });
  }

  bodyMembers(flag) {
    return this.state.miembros.map((currentMember, i) => {
      return (
        <tr key={i}>
          <td>{currentMember.data.nombre}</td>
          {this.auxFunctionBox(
            currentMember.id,
            currentMember.data.liderUID,
            flag
          )}
          {/* 0 es para que tomar lista - 1 es para que sea read-only*/}
        </tr>
      );
    });
  }

  getCantFaltas(liderUID, idFecha, idDia) {
    // console.log(liderUID,idFecha,idDia);
    var listaFiltrada = this.state.asistencias.filter(
      asistencia =>
        // asistencia.data.idMember === idMember &&
        asistencia.data.uidLider === liderUID &&
        asistencia.data.idFecha === idFecha &&
        asistencia.data.idDia === idDia &&
        asistencia.data.asis === false
    );
    // console.log(listaFiltrada);
    return listaFiltrada.length;
  }

  totalFaltas(liderUID) {
    var lista = this.state.fechas.filter(
      fecha => fecha.data.idDia === this.state.diaSelected
    );
    return lista.map((currentDate, i) => {
      return (
        <td key={i}>
          {this.getCantFaltas(liderUID, currentDate.id, currentDate.data.idDia)}
        </td>
      );
    });
  }

  resumenFechas() {
    // console.log(this.state.lider);
    return (
      <tr>
        <td>Total Faltas: </td>
        {this.totalFaltas(this.state.lider.uid)}
      </tr>
    );
  }

  bodyTable() {
    // var inicio = this.bodyMembers(1);
    var fin = this.resumenFechas();
    return (
      <tbody>
        {/* {inicio} */}
        {fin}
      </tbody>
    );
  }

  render() {
    
    return (
      <div className="container-att">
        {/* <hr /> */}
        <h1 className="uno-color">Dias de Célula</h1>
        <br />
        <div className="table-responsive">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Dia</th>
                <th>Detalles</th>
                <th>Asistencia</th>
              </tr>
            </thead>
            <tbody>{this.diasList()}</tbody>
          </table>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          contentLabel="Add user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color">Asistencia de Miembros</h1>
            <div className="table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    {this.headersFecha()}
                  </tr>
                </thead>
                <tbody>{this.bodyMembers(0)}</tbody>
              </table>
            </div>
            <button
              type="button"
              className="btn salir-btn size-small"
              onClick={this.closeModal}
            >
              Salir
            </button>
          </div>
        </ReactModal>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModalDetail}
          contentLabel="Detail class"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color">DETALLE DE ASISTENCIA</h1>
            <div className="table-responsive">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    {this.headersFecha()}
                  </tr>
                </thead>
                {this.bodyTable()}
              </table>
            </div>
            <button
              type="button"
              className="btn salir-btn size-small"
              onClick={this.closeModalDetail}
            >
              Salir
            </button>
          </div>
        </ReactModal>
      </div>
    );
  }
}
