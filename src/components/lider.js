import React, { Component } from "react";
import firebase from "firebase";
import swal from "sweetalert2";
import ReactModal from "react-modal";
import Attendace from "./attendance";

import "../styles/lider.css";

export default class Lider extends Component {
  constructor(props) {
    super(props);

    this.firebase = firebase;
    this.swal = swal;

    this.addUser = this.addUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModalUp = this.openModalUp.bind(this);
    this.closeModalUp = this.closeModalUp.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.verificarCorreoExistente = this.verificarCorreoExistente.bind(this);
    this.cargarUsuarios = this.cargarUsuarios.bind(this);

    this.state = {
      usuario: this.props.usuario,
      usuarios: [],
      showModal: false,
      showModalUp: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      idUpdate: ""
    };

    this.cargarUsuarios();
  }

  async cargarUsuarios() {
    await firebase
      .firestore()
      .collection("usuarios")
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          if (
            doc.data().liderUID === this.state.usuario.uid &&
            doc.data().rol === "miembro" &&
            doc.data().eliminado !== 1
          ) {
            var obj = {
              id: doc.id,
              data: doc.data()
            };
            this.setState({
              usuarios: [...this.state.usuarios, obj]
            });
            return;
          }
        });
      });
  }

  // async componentDidMount() {
  //   await this.cargarUsuarios();
  // }

  handleChange(e) {
    const value = e.target.value;
    this.setState({ ...this.state, [e.target.name]: value });
  }

  openModal() {
    this.setState({ showModal: true });
  }

  openModalUp(id) {
    console.log("UPDATE USER");
    console.log(id);
    var obj = this.state.usuarios.find(user => user.id === id);
    this.setState({
      idUpdate: id,
      showModalUp: true,
      nombreNU: obj.data.nombre,
      apellidoNU: obj.data.apellido,
      edadNU: obj.data.edad,
      direccionNU: obj.data.direccion,
      telefonoNU: obj.data.telefono
    });
  }

  closeModal() {
    this.setState({
      showModal: false,
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: ""
    });
  }

  closeModalUp() {
    this.setState({
      showModalUp: false,
      idUpdate: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: ""
    });
  }

  verificarCorreoExistente() {
    var auxExiste =
      this.state.usuarios.some(
        item =>
          this.state.usuarioNU === item.data.usuario &&
          item.data.eliminado === 0
      ) || this.state.usuario.usuario === this.state.usuarioNU;
    return auxExiste;
  }

  async hallarUsuario(userEmail) {
    var dato = await this.firebase
      .firestore()
      .collection("usuarios")
      .where("usuario", "==", userEmail)
      .get();
    var obj = {
      id: dato.docs[0].id,
      data: dato.docs[0].data()
    };
    // return dato.docs[0];
    return obj;
  }

  async addUser() {
    var fechaIn = new Date();
    fechaIn = fechaIn.toLocaleString("es-ES", {
      dateStyle: "short",
      timeStyle: "short"
    });
    if (this.verificarCorreoExistente()) {
      this.swal.fire({
        title: "Error",
        text: "Miembro ingresado ya existe",
        icon: "error"
      });
    } else {
      await this.firebase
        .firestore()
        .collection("usuarios")
        .add({
          nombre: this.state.nombreNU,
          apellido: this.state.apellidoNU,
          edad: this.state.edadNU,
          direccion: this.state.direccionNU,
          telefono: this.state.telefonoNU,
          fechaIngreso: fechaIn,
          usuario: this.state.usuarioNU,
          rol: "miembro",
          liderUID: this.state.usuario.uid,
          eliminado: 0
        })
        .then(async () => {
          var miembroNuevo = await this.hallarUsuario(this.state.usuarioNU);
          var auxUsuarios = [...this.state.usuarios];
          auxUsuarios.push(miembroNuevo);
          this.setState({ usuarios: auxUsuarios });
          this.swal.fire({
            title: "Aviso",
            text: "Registro Exitoso",
            icon: "success",
            timer: 2000
          });
          this.closeModal();
        })
        .catch(error => {
          console.log(error.message);
          this.swal.fire({
            title: "Error",
            text: error.message,
            icon: "error",
            timer: 12000
          });
        });
    }
  }

  async deleteUser(id) {
    console.log("DELETE USER: " + id);
    await firebase
      .firestore()
      .collection("usuarios")
      .doc(id)
      .update({
        eliminado: 1
      })
      .then(() => {
        var index = this.state.usuarios
          .map(user => {
            return user.id;
          })
          .indexOf(id);
        var auxUsuarios = [...this.state.usuarios];
        if (index !== -1) {
          auxUsuarios.splice(index, 1);
          this.setState({ usuarios: auxUsuarios });
          this.swal.fire({
            title: "Aviso",
            text: "Borrado correcto",
            icon: "success"
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.swal.fire({
          title: "Alerta",
          text: e.msg,
          icon: "error"
        });
      });
  }

  async updateUser() {
    await this.firebase
      .firestore()
      .collection("usuarios")
      .doc(this.state.idUpdate)
      .update({
        nombre: this.state.nombreNU,
        apellido: this.state.apellidoNU,
        edad: this.state.edadNU,
        direccion: this.state.direccionNU,
        telefono: this.state.telefonoNU
      })
      .then(() => {
        var index = this.state.usuarios
          .map(user => {
            return user.id;
          })
          .indexOf(this.state.idUpdate);
        var auxUsuarios = [...this.state.usuarios];
        if (index !== -1) {
          auxUsuarios[index].data.nombre = this.state.nombreNU;
          auxUsuarios[index].data.apellido = this.state.apellidoNU;
          auxUsuarios[index].data.edad = this.state.edadNU;
          auxUsuarios[index].data.direccion = this.state.direccionNU;
          auxUsuarios[index].data.telefono = this.state.telefonoNU;
          // console.log("USUARIO: " + auxUsuarios[index].usuario);
          // console.log("NOMBRE: " + auxUsuarios[index].nombre);
          this.setState({ usuarios: auxUsuarios });
          this.swal.fire({
            title: "Aviso",
            text: "Actualización correcto",
            icon: "success"
          });
          this.closeModalUp();
        }
      });
  }

  usuarioList() {
    return this.state.usuarios.map((currentusuario, i) => {
      return (
        <tr key={i}>
          <td>
            {currentusuario.data.nombre +
              " " +
              (currentusuario.data.apellido
                ? currentusuario.data.apellido
                : "")}
          </td>
          <td>{currentusuario.data.usuario}</td>
          <td>
            <button
              type="button"
              className="btn eliminar-btn"
              onClick={() => {
                this.deleteUser(currentusuario.id);
              }}
            >
              Eliminar
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn actualizar-btn"
              onClick={() => {
                this.openModalUp(currentusuario.id);
              }}
            >
              Actualizar
            </button>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="container-lider">
        <div className="row">
          <div className="col-9">
            <h1 className="uno-color">Lista de Miembros</h1>
          </div>
          <div className="col-3">
            <label className="efecto-hover" onClick={this.openModal}>
              <u>Agregar Miembro</u>
            </label>
          </div>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          contentLabel="Add user"
          style={{ height: "25%", width: "25%" }}
        >
          <div className="modal-div">
            <h1 className="uno-color">Registro de Nuevo Miembro</h1>
            <hr />
            <div className="row">
              <div className="col-6">
                <label className="fuente">Nombre: </label>
                <input
                  type="text"
                  required
                  name="nombreNU"
                  className="form-control"
                  value={this.state.nombreNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-6">
                <label className="fuente">Apellido: </label>
                <input
                  type="text"
                  required
                  name="apellidoNU"
                  className="form-control"
                  value={this.state.apellidoNU}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Dirección: </label>
              <input
                type="text"
                required
                name="direccionNU"
                className="form-control sangria"
                value={this.state.direccionNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row">
              <div className="col-6">
                <label className="fuente">Edad: </label>
                <input
                  type="number"
                  required
                  name="edadNU"
                  className="form-control"
                  value={this.state.edadNU}
                  onChange={this.handleChange}
                />
              </div>
              <div className="col-6">
                <label className="fuente">Teléfono: </label>
                <input
                  type="text"
                  required
                  name="telefonoNU"
                  className="form-control"
                  value={this.state.telefonoNU}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="row">
              <label className="fuente sangria">Usuario: </label>
              <input
                type="text"
                required
                name="usuarioNU"
                className="form-control sangria"
                value={this.state.usuarioNU}
                onChange={this.handleChange}
              />
            </div>
            <div className="row botones">
              <button
                type="button"
                className="btn guardar-btn"
                onClick={this.addUser}
              >
                Guardar
              </button>
              <button
                type="button"
                className="btn salir-btn"
                onClick={this.closeModal}
              >
                Cerrar
              </button>
            </div>
          </div>
        </ReactModal>
        <div className="table-responsive">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Eliminar</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>{this.usuarioList()}</tbody>
          </table>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModalUp}
          contentLabel="Update user"
          style={{ height: "25%", width: "25%" }}
        >
          <h1>Actualizar Miembro</h1>
          <hr />
          <form>
            <label>Nombre: </label>
            <input
              type="text"
              required
              name="nombreNU"
              className="form-control"
              defaultValue={this.state.nombreNU || ""}
              onChange={this.handleChange}
            />
            <label>Apellido: </label>
            <input
              type="text"
              required
              name="apellidoNU"
              className="form-control"
              defaultValue={this.state.apellidoNU || ""}
              onChange={this.handleChange}
            />
            <label>Edad: </label>
            <input
              type="number"
              required
              name="edadNU"
              className="form-control"
              defaultValue={this.state.edadNU || 0}
              onChange={this.handleChange}
            />
            <label>Dirección: </label>
            <input
              type="text"
              required
              name="direccionNU"
              className="form-control"
              defaultValue={this.state.direccionNU || ""}
              onChange={this.handleChange}
            />
            <label>Telefono: </label>
            <input
              type="text"
              required
              name="telefonoNU"
              className="form-control"
              defaultValue={this.state.telefonoNU || ""}
              onChange={this.handleChange}
            />
          </form>
          <br />
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.updateUser}
          >
            Guardar cambios
          </button>

          <button
            type="button"
            className="btn btn-outline-secondary"
            onClick={this.closeModalUp}
          >
            Cerrar
          </button>
        </ReactModal>
        {/* <Attendace lider={this.state.usuario} /> */}
        <Attendace lider={this.state.usuario} miembros={this.state.usuarios} />
      </div>
    );
  }
}
