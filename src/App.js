import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Login from "./components/login"
import Home from "./components/home"
import Attendance from "./components/attendance"

function App() {
  return (
    <Router>
        <Route path="/" exact component={Login} />
        <Route path="/home" component={Home} />
        <Route path="/attendance" component={Attendance} />
    </Router>
  );
}

export default App;
