import React, { Component } from "react";

export default class Historial extends Component {
  render() {
    return (
      <div>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li class="breadcrumb-item">
              Home
            </li>
            <li className="breadcrumb-item">
              Library
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              Data
            </li>
          </ol>
        </nav>
      </div>
    );
  }
}
