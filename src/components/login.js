import React, { Component } from "react";
import firebase from "firebase";
import swal from "sweetalert2";
import "../styles/login.css";
import logo from "../assets/logo.jpg"

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.onChangeUsuario = this.onChangeUsuario.bind(this);
    this.onChangeContrasena = this.onChangeContrasena.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.logueo = this.logueo.bind(this);
    this.logueoGoogle = this.logueoGoogle.bind(this);
    this.logueoFb = this.logueoFb.bind(this);

    this.firebase = firebase;
    this.swal = swal;

    this.state = {
      usuario: "",
      contrasena: ""
    };
  }

  onChangeUsuario(e) {
    this.setState({
      usuario: e.target.value
    });
  }

  onChangeContrasena(e) {
    this.setState({
      contrasena: e.target.value
    });
  }

  onKeyDown(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      event.stopPropagation();
      this.logueo();
    }
  }

  setUsuarioUid(currentUser) {
    this.firebase
      .firestore()
      .collection("usuarios")
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          if (
            doc.data().usuario === currentUser.email &&
            doc.data().uid === ""
          ) {
            console.log(doc);
            let usuarioHallado = this.firebase
              .firestore()
              .collection("usuarios")
              .doc(doc.id);
            usuarioHallado.update({
              uid: currentUser.uid
            });
          }
        });
      });
  }

  logueo() {
    if (this.state.usuario === "" || this.state.contrasena === "") {
      this.swal.fire({
        title: "Error",
        text: "Error en logueo: Complete los datos",
        icon: "error"
      });
    } else {
      this.firebase
        .auth()
        .signInWithEmailAndPassword(this.state.usuario, this.state.contrasena)
        .then(data => {
          console.log(data);
          this.setUsuarioUid(data.user);
          if (data.user.displayName === "") {
            data.user.updateProfile({
              displayName: data.user.displayName
            });
          }

          this.swal.fire({
            title: "Aviso",
            text: "Ingreso correcto ",
            icon: "success",
            timer: 2000
          });
          this.props.history.push("/home");
        })
        .catch(err => {
          this.swal.fire({
            title: "Error",
            text: "Error en logueo: " + err.message,
            icon: "error"
          });
        });
    }
  }

  logueoFb() {
    const provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(result => {
        // var aux = result.user;
        // console.log(JSON.stringify(aux));
        this.swal.fire({
          title: "Aviso",
          text: "Logueo Exitoso",
          timer: 2000,
          icon: "success"
        });
        this.props.history.push("/home");
      })
      .catch(error => {
        var errorMessage = error.message;
        this.swal.fire({
          title: "Error",
          text: "Fallo en conectar con facebook: " + errorMessage,
          timer: 2000,
          icon: "error"
        });
      });
  }

  logueoGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(result => {
        // var aux = result.user;
        // console.log(JSON.stringify(aux));
        this.swal.fire({
          title: "Aviso",
          text: "Logueo Exitoso",
          timer: 2000,
          icon: "success"
        });
        this.props.history.push("/home");
      })
      .catch(error => {
        var errorMessage = error.message;
        this.swal.fire({
          title: "Error",
          text: "Fallo en conectar con google: " + errorMessage,
          timer: 2000,
          icon: "error"
        });
      });
  }

  render() {
    return (
      <div className="container container-login">
        <form>
          <img src={logo} alt="Logo"></img>
          <h3>Bienvenido</h3>
          <div className="form-group">
            <input
              type="text"
              required
              className="form-control entradas"
              placeholder="Correo"
              value={this.state.usuario}
              onChange={this.onChangeUsuario}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              required
              className="form-control entradas"
              placeholder="Contraseña"
              value={this.state.contrasena}
              onChange={this.onChangeContrasena}
              onKeyDown={this.onKeyDown}
            />
          </div>
          <div className="form-group grupo-botones">
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.logueo}
            >
              Login
            </button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.logueoFb}
            >
              Facebook
            </button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.logueoGoogle}
            >
              Google
            </button>
          </div>
        </form>
      </div>
    );
  }
}
