import React, { Component } from "react";
import firebase from "firebase";
import swal from "sweetalert2";
import Navbar from "./navbar";
import Lider from "./lider";
import Admin from "./admin";
import "../styles/home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.mounted = false;

    this.firebase = firebase;
    this.swal = swal;
    this.logout = this.logout.bind(this);

    this.state = {
      usuarioLog: {
        contrasena: "",
        eliminado: "",
        nombre: "",
        rol: "",
        uid: "",
        usuario: ""
      }
    };
  }

  async getCurrentUser() {
    try {
      var currentUser = await this.firebase.auth().currentUser;
      return currentUser;
    } catch (error) {
      console.log(error.message);
    }
  }

  async getUsuarioLog() {
    let snapshot = await this.firebase
      .firestore()
      .collection("usuarios")
      .get();
    var currentUser = await this.getCurrentUser();
    if (currentUser === null) this.props.history.push("/");
    else {
      snapshot.forEach(doc => {
        if (
          doc.data().usuario === currentUser.email &&
          doc.data().uid === currentUser.uid
        ) {
          this.setState({ usuarioLog: doc.data() });
        }
      });
    }
  }

  componentDidMount() {
    this.getUsuarioLog();
  }

  logout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.history.push("/");
      });
  }

  render() {
    if (this.state.usuarioLog.nombre === "")
      return (
        <div className="d-flex justify-content-center">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      );

    const esAdmin = this.state.usuarioLog.rol === "admin";

    return (
      <div className="container-home">
        <Navbar
          nombre={this.state.usuarioLog.nombre}
          clickeo={() => this.logout()}
        />
        <div style={{ margin: "5px" }}>
          {esAdmin ? (
            <Admin usuario={this.state.usuarioLog} />
          ) : (
            <Lider usuario={this.state.usuarioLog} />
          )}
        </div>
      </div>
    );
  }
}
