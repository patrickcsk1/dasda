import React, { Component } from "react";
import firebase from "firebase";
import firebase2 from "firebase";
import swal from "sweetalert2";
import ReactModal from "react-modal";

import "../styles/admin.css";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class Admin extends Component {
  constructor(props) {
    super(props);

    this.firebase = firebase;
    this.firebase2 = firebase2;
    this.swal = swal;

    this.addUser = this.addUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModalUp = this.openModalUp.bind(this);
    this.closeModalUp = this.closeModalUp.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDias = this.handleChangeDias.bind(this);

    this.verificarCorreoExistente = this.verificarCorreoExistente.bind(this);
    this.cargarUsuarios = this.cargarUsuarios.bind(this);

    this.state = {
      startDate: new Date(),
      // endDate: new Date(),
      usuario: this.props.usuario,
      usuarios: [],
      showModal: false,
      showModalUp: false,
      cantSesiones: 0,
      uidNU: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      rolNU: "admin",
      contraNU: "",
      idUpdate: ""
    };
  }

  cargarUsuarios() {
    firebase
      .firestore()
      .collection("usuarios")
      .orderBy("nombre")
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          if (
            doc.data().usuario !== this.state.usuario.email &&
            doc.data().uid !== this.state.usuario.uid &&
            doc.data().rol !== "miembro" &&
            doc.data().eliminado !== 1
          ) {
            var obj = {
              id: doc.id,
              data: doc.data()
            };
            this.setState({
              usuarios: [...this.state.usuarios, obj]
            });
            return;
          }
        });
      });
  }

  componentDidMount() {
    this.cargarUsuarios();
  }

  handleChange(e) {
    const value = e.target.value;
    this.setState({ ...this.state, [e.target.name]: value });
  }

  handleChangeDias(dateIn) {
    this.setState({
      startDate: dateIn
    });
  }

  obtenerDiaEs(dia) {
    if (dia === "Mon") return "lunes";
    else if (dia === "Tue") return "martes";
    else if (dia === "Wed") return "miercoles";
    else if (dia === "Thu") return "jueves";
    else if (dia === "Fri") return "viernes";
    else if (dia === "Sat") return "sabado";
    else if (dia === "Sun") return "domingo";
  }

  async guardarFecha(fechaIn, idDiaIn) {
    console.log(fechaIn);
    var fechaHallado = await this.firebase
      .firestore()
      .collection("fechas")
      .where("fecha", "==", fechaIn)
      .where("idDia", "==", idDiaIn)
      .get();
    if (fechaHallado.empty) {
      await this.firebase
        .firestore()
        .collection("fechas")
        .add({
          fecha: fechaIn,
          idDia: idDiaIn
        })
        .then(() => {
          this.swal.fire({
            title: "Aviso",
            text: "Registro de fecha exitoso",
            icon: "success",
            timer: 12000
          });
        });
    } else {
      this.swal.fire({
        title: "Aviso",
        text: "Dia repetido " + fechaIn,
        icon: "warning",
        timer: 12000
      });
    }
  }

  async confirmarClases() {
    console.log(this.state.cantSesiones, this.state.startDate);
    var dateIn = this.state.startDate;
    var auxDia = dateIn.toString().split(" ")[0];
    var dia = this.obtenerDiaEs(auxDia);

    var diaHallado = await this.firebase
      .firestore()
      .collection("dias")
      .where("descripcion", "==", dia)
      .get();
    if (diaHallado.empty) {
      await this.firebase
        .firestore()
        .collection("dias")
        .add({
          descripcion: dia
        })
        .then(async ref => {
          var cont = 0;
          while (cont < this.state.cantSesiones) {
            var shortDate = dateIn.toLocaleString("es-ES", {
              dateStyle: "short"
            });
            console.log(shortDate);
            await this.guardarFecha(shortDate, ref.id);
            dateIn.setDate(dateIn.getDate() + 7);
            cont++;
          }
          this.swal.fire({
            title: "Aviso",
            text: "Registro de dia exitoso",
            icon: "success",
            timer: 12000
          });
        })
        .catch(e => console.log("Registro dia error: " + e.message));
    } else {
      this.swal.fire({
        title: "Aviso",
        text: "Dia repetido",
        icon: "warning",
        timer: 12000
      });
    }
  }

  openModal() {
    this.setState({ showModal: true, showModalUp: false });
  }

  openModalUp(id) {
    var obj = this.state.usuarios.find(user => user.id === id);
    this.setState({
      idUpdate: id,
      showModalUp: true,
      showModal: false,
      nombreNU: obj.data.nombre,
      apellidoNU: obj.data.apellido,
      edadNU: obj.data.edad,
      direccionNU: obj.data.direccion,
      telefonoNU: obj.data.telefono
    });
  }

  closeModal() {
    this.setState({
      showModal: false,
      uidNU: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: "",
      usuarioNU: "",
      rolNU: "admin",
      contraNU: ""
    });
  }

  closeModalUp() {
    this.setState({
      showModalUp: false,
      idUpdate: "",
      nombreNU: "",
      apellidoNU: "",
      edadNU: "",
      direccionNU: "",
      telefonoNU: ""
    });
  }

  verificarCorreoExistente() {
    var auxExiste =
      this.state.usuarios.some(
        item =>
          this.state.usuarioNU === item.data.usuario &&
          item.data.eliminado === 0
      ) || this.state.usuario.usuario === this.state.usuarioNU;
    return auxExiste;
  }

  async hallarUsuario(uidUsuario) {
    var dato = await this.firebase
      .firestore()
      .collection("usuarios")
      .where("uid", "==", uidUsuario)
      .get();
    var obj = {
      id: dato.docs[0].id,
      data: dato.docs[0].data()
    };
    // return dato.docs[0];
    return obj;
  }

  async guardarUsuario() {
    var fechaIn = new Date();
    fechaIn = fechaIn.toLocaleString("es-ES", {
      dateStyle: "short",
      timeStyle: "short"
    });
    console.log(fechaIn);
    await this.firebase
      .firestore()
      .collection("usuarios")
      .add({
        uid: this.state.uidNU,
        nombre: this.state.nombreNU,
        apellido: this.state.apellidoNU,
        edad: this.state.edadNU,
        direccion: this.state.direccionNU,
        telefono: this.state.telefonoNU,
        fechaIngreso: fechaIn,
        usuario: this.state.usuarioNU,
        rol: this.state.rolNU,
        contrasena: this.state.contraNU,
        eliminado: 0
      })
      .then(async () => {
        var auxUsuarios = [...this.state.usuarios];
        var usuarioNuevo = await this.hallarUsuario(this.state.uidNU);
        auxUsuarios.push(usuarioNuevo);
        this.setState({ usuarios: auxUsuarios });
        this.swal.fire({
          title: "Aviso",
          text: "Registro Exitoso",
          icon: "success",
          timer: 12000
        });
      })
      .catch(error => {
        console.log(error.message);
        this.swal.fire({
          title: "Error",
          text: error.message,
          icon: "error",
          timer: 12000
        });
      });
  }

  async addUser() {
    if (this.verificarCorreoExistente()) {
      this.swal.fire({
        title: "Error",
        text: "Usuario ingresado ya existe",
        icon: "error"
      });
    } else {
      await this.firebase2
        .auth()
        .createUserWithEmailAndPassword(
          this.state.usuarioNU,
          this.state.contraNU
        )
        .then(async data => {
          this.setState({ uidNU: data.user.uid });
          await this.guardarUsuario();
          await this.firebase2.auth().signOut();
        })
        .catch(error => {
          console.log(error.message);
          this.swal.fire({
            title: "Warning",
            text: error.message,
            icon: "error",
            timer: 12000
          });
        });
      this.closeModal();
    }
  }

  deleteUser(id) {
    console.log("DELETE USER");
    console.log(id);
    this.firebase
      .firestore()
      .collection("usuarios")
      .doc(id)
      .update({
        eliminado: 1
      })
      .then(() => {
        var index = this.state.usuarios
          .map(usuario => {
            return usuario.id;
          })
          .indexOf(id);
        var auxUsuarios = [...this.state.usuarios];
        if (index !== -1) {
          auxUsuarios.splice(index, 1);
          this.setState({ usuarios: auxUsuarios });
          this.swal.fire({
            title: "Aviso",
            text: "Borrado correcto",
            icon: "success"
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.swal.fire({
          title: "Alerta",
          text: e.msg,
          icon: "error"
        });
      });
  }

  updateUser() {
    this.firebase
      .firestore()
      .collection("usuarios")
      .doc(this.state.idUpdate)
      .update({
        nombre: this.state.nombreNU,
        apellido: this.state.apellidoNU,
        edad: this.state.edadNU,
        direccion: this.state.direccionNU,
        telefono: this.state.telefonoNU
      })
      .then(() => {
        var index = this.state.usuarios
          .map(user => {
            return user.id;
          })
          .indexOf(this.state.idUpdate);
        var auxUsuarios = [...this.state.usuarios];
        if (index !== -1) {
          auxUsuarios[index].data.nombre = this.state.nombreNU;
          auxUsuarios[index].data.apellido = this.state.apellidoNU;
          auxUsuarios[index].data.edad = this.state.edadNU;
          auxUsuarios[index].data.direccion = this.state.direccionNU;
          auxUsuarios[index].data.telefono = this.state.telefonoNU;
          this.setState({ usuarios: auxUsuarios });
          this.swal.fire({
            title: "Aviso",
            text: "Actualización correcto",
            icon: "success"
          });
          this.closeModalUp();
        }
      });
  }

  usuarioList() {
    return this.state.usuarios.map((currentusuario, i) => {
      return (
        <tr key={i}>
          <td>{currentusuario.data.nombre}</td>
          <td>{currentusuario.data.usuario}</td>
          <td>{currentusuario.data.rol.toUpperCase()}</td>
          <td>
            <button
              type="button"
              className="btn eliminar-btn"
              onClick={() => {
                this.deleteUser(currentusuario.id);
              }}
            >
              Delete
            </button>
          </td>
          <td>
            <button
              type="button"
              className="btn actualizar-btn"
              onClick={() => {
                this.openModalUp(currentusuario.id);
              }}
            >
              Update
            </button>
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="container-admin">
        <div className="row">
          <div className="col-9">
            <h1 className="uno-color">Lista de Usuarios</h1>
          </div>
          <div className="col-3">
            <label onClick={this.openModal} className="efecto-hover">
              <u>Agregar Usuario</u>
            </label>
          </div>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModal}
          contentLabel="Add user"
          style={{ height: "25%", width: "25%" }}
        >
          <h1>Registro de Nuevo Usuario</h1>
          <hr />
          <form>
            <label>Nombre: </label>
            <input
              type="text"
              required
              name="nombreNU"
              className="form-control"
              value={this.state.nombreNU}
              onChange={this.handleChange}
            />
            <label>Apellido: </label>
            <input
              type="text"
              required
              name="apellidoNU"
              className="form-control"
              value={this.state.apellidoNU}
              onChange={this.handleChange}
            />
            <label>Edad: </label>
            <input
              type="number"
              required
              name="edadNU"
              className="form-control"
              value={this.state.edadNU}
              onChange={this.handleChange}
            />
            <label>Dirección: </label>
            <input
              type="text"
              required
              name="direccionNU"
              className="form-control"
              value={this.state.direccionNU}
              onChange={this.handleChange}
            />
            <label>Teléfono: </label>
            <input
              type="text"
              required
              name="telefonoNU"
              className="form-control"
              value={this.state.telefonoNU}
              onChange={this.handleChange}
            />
            <label>Usuario: </label>
            <input
              type="text"
              required
              name="usuarioNU"
              className="form-control"
              value={this.state.usuarioNU}
              onChange={this.handleChange}
            />
            <label>Contraseña: </label>
            <input
              type="password"
              required
              name="contraNU"
              className="form-control"
              value={this.state.contraNU}
              onChange={this.handleChange}
            />
            <label>Seleccione el rol:</label>
            <select
              className="form-control"
              value={this.state.rolNU}
              name="rolNU"
              onChange={this.handleChange}
            >
              <option value="admin">Admin</option>
              <option value="lider">Lider</option>
            </select>
          </form>
          <br />
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.addUser}
          >
            Guardar
          </button>

          <button
            type="button"
            className="btn btn-outline-secondary"
            onClick={this.closeModal}
          >
            Cerrar
          </button>
        </ReactModal>
        <div className="table-responsive scroll">
          <table className="table table-hover table-sm">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Eliminar</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
              {this.state.usuarios.length !== 0 ? (
                this.usuarioList()
              ) : (
                <tr>
                  <td colSpan="5">
                    <div style={{ textAlign: "center" }}>
                      No hay usuarios registrados
                    </div>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={this.state.showModalUp}
          contentLabel="Update user"
          style={{ height: "25%", width: "25%" }}
        >
          <h1>Actualizar Usuario</h1>
          <hr />
          <form>
            <label>Nombre: </label>
            <input
              type="text"
              required
              name="nombreNU"
              className="form-control"
              defaultValue={this.state.nombreNU || ""}
              onChange={this.handleChange}
            />
            <label>Apellido: </label>
            <input
              type="text"
              required
              name="apellidoNU"
              className="form-control"
              defaultValue={this.state.apellidoNU || ""}
              onChange={this.handleChange}
            />
            <label>Edad: </label>
            <input
              type="number"
              required
              name="edadNU"
              className="form-control"
              defaultValue={this.state.edadNU || 0}
              onChange={this.handleChange}
            />
            <label>Dirección: </label>
            <input
              type="text"
              required
              name="direccionNU"
              className="form-control"
              defaultValue={this.state.direccionNU || ""}
              onChange={this.handleChange}
            />
            <label>Telefono: </label>
            <input
              type="text"
              required
              name="telefonoNU"
              className="form-control"
              defaultValue={this.state.telefonoNU || ""}
              onChange={this.handleChange}
            />
          </form>
          <br />
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.updateUser}
          >
            Guardar cambios
          </button>

          <button
            type="button"
            className="btn btn-seccundary"
            onClick={this.closeModalUp}
          >
            Cerrar
          </button>
        </ReactModal>
        <hr />
        <div className="dia-celula">
          <h3>Registro de días de cédulas</h3>
          <hr />
          <div className="row">
            <div className="col-4">
              <label>Seleccione el dia inicial de la celula:</label>
            </div>
            <div className="col-4">
              <label>Cantidad de sesiones:</label>
            </div>
            <div className="col-4"></div>
          </div>
          <div className="row">
            <div className="col-4">
              <DatePicker
                className="fecha"
                name="startDate"
                selected={this.state.startDate}
                onChange={this.handleChangeDias}
                dateFormat="dd/MM/yyyy"
                placeholderText="Seleccione dia inicial"
              />
            </div>
            <div className="col-4">
              <input
                type="number"
                required
                placeholder="Nro sesiones"
                name="cantSesiones"
                className="form-control"
                value={this.state.cantSesiones}
                onChange={this.handleChange}
              />
            </div>
            <div className="col-4">
              <button
                type="button"
                className="btn actualizar-btn"
                onClick={() => this.confirmarClases()}
              >
                Aceptar
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
